#!/bin/sh
config_file="$HOME/.config/bspwm/stickies_config"
term="alacritty"

get_metadata() {
	if [ "$1" = "all" ]; then
		grep --invert "#" "$config_file" \
			| awk '{ print $1,$2,$3,$4 }'
	else
		grep "^$1" "$config_file" \
			| awk '{ print $1,$2,$3,$4 }'
	fi
}
dump_id() {
	bspc query -N -n .sticky \
		| xargs -i sh -c \
		"bspc query --node {} -T | grep -q $1 && echo {}" > $2
}
stickie() {
	if [ $(ps -x | grep -c "$1.*$3") -eq "1" ]; then
		$term --title "$1" --class "$1" -e "$3" &
		while [ ! $(bspc query -N -n .sticky) ]; do sleep 0.5; done
		dump_id $1 $2
	else
		while [ ! $(cat "$2" 2>/dev/null) ]; do dump_id $1 $2; done
		id=$(cat "$2")
		bspc node "$id" --flag hidden
		bspc node -f "$id"
	fi
}

get_metadata "$1" | while IFS=' ' read -r name pid_file cmd enabled
do
	if [ "$enabled" = "true" ]; then
 		bspc rule -a "*:$name" state=floating sticky=on hidden=on
		stickie "$name" "$pid_file" "$cmd"
	fi
done
