--   _______         .__
--   \      \ ___  __|__|  _____ _______   ____
--   /   |   \\  \/ /|  | /     \\_  __ \_/ ___\
--  /    |    \\   / |  ||  Y Y  \|  | \/\  \___
--  \____|__  / \_/  |__||__|_|  /|__|    \___  >
--          \/                 \/             \/
--
-- Author: MoonMaker72
-- Repo: https://codeberg.org/MoonMaker72/MoonDots

local o = vim.o
local cmd = vim.cmd
local g = vim.g
local opt = vim.opt

o.incsearch = true
o.encoding = 'utf-8'
o.foldlevel = 99
o.scrolloff = 8
o.tabstop = 4
o.shiftwidth = 4
o.splitright = true
o.list = true
o.splitbelow = true
o.hidden = true
o.cmdheight = 1
o.updatetime = 300
o.inccommand = 'split'
o.conceallevel = 2
opt.listchars = { tab = '  ┊', trail = '~' }
opt.shortmess:append('c')
cmd("set clipboard+=unnamedplus")
cmd("filetype plugin indent on")
g.mapleader="\\"

g.loaded_gzip = false
g.loaded_netrwPlugin = false
g.loaded_tarPlugin = false
g.loaded_zipPlugin = false
g.loaded_2html_plugin = false
g.loaded_remote_plugins = false

require('plugins')
require('keybinds')
require('colorscheme')
