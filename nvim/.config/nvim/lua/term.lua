require("toggleterm").setup {
	size = function(term)
		if term.direction == "horizontal" then
			return 15
		elseif term.direction == "vertical" then
			return vim.o.columns * 0.4
		end
	end,
	open_mapping = [[<c-\>]],
	hide_numbers = true,
	shade_terminals = false,
	insert_mappings = true,
	close_om_exit = true,
	direction = 'float',
	float_opts = {
		border = 'single',
	}
}
