local db = require('dashboard')

db.custom_center ={
	{
		icon = "ﱐ ",
		desc = 'New File',
		action = 'DashboardNewFile',
	},
	{
		icon = " ",
		desc = 'Find File',
		action = ':Telescope find_files',
	},
	{
		icon = " ",
		desc = 'Find Histry',
		action = ':Telescope oldfiles',
	},
	{
		icon = " ",
		desc = 'Live Grep',
		action = ':Telescope live_grep',
	},
	{
		icon = " ",
		desc = 'Cofiguration',
		action = ':e $MYVIMRC',
	},
	{
		icon = " ",
		desc = 'Sync Plugins',
		action = ':PackerSync',
	},
	{
		icon = " ",
		desc = 'Update Parsers',
		action = ':TSUpdateSync',
	},
}

db.custom_header = {
'   _____    _______      _____  __________ _________    ___ ___  .___    _____',
'  /  _  \\   \\      \\    /  _  \\ \\______   \\\\_   ___ \\  /   |   \\ |   |  /  _  \\',
' /  /_\\  \\  /   |   \\  /  /_\\  \\ |       _//    \\  \\/ /    ~    \\|   | /  /_\\  \\',
'/    |    \\/    |    \\/    |    \\|    |   \\\\     \\____\\    Y    /|   |/    |    \\',
'\\____|__  /\\____|__  /\\____|__  /|____|_  / \\______  / \\___|_  / |___|\\____|__  /',
'        \\/         \\/         \\/        \\/         \\/        \\/               \\/',
}
db.custom_footer = {
	'[ I am the proprietor of Anarchy ]',
}

-- db.preview_file_Path    -- string or function type that mean in function you can dynamic generate height width
-- db.preview_file_height  -- number type
-- db.preview_file_width   -- number type
-- db.preview_command      -- string type (can be ueberzug which only work in linux)
-- db.confirm_key          -- string type key that do confirm in center select
-- db.session_directory    -- string type the directory to store the session file
-- db.header_pad           -- number type default is 1
-- db.center_pad           -- number type default is 1
-- db.footer_pad           -- number type default is 1
db.hide_statusline = true
db.hide_tabline = true

vim.cmd[[hi link DashboardHeader PurpleItalic]]
vim.cmd[[hi link DashboardCenter Orange]]
vim.cmd[[hi link DashboardFooter BlueItalic]]
