require('packer').startup(function(use)

	-- Packer
	use { 'wbthomason/packer.nvim' }

	-- Colour Stuff
	use { 'norcalli/nvim-colorizer.lua', cmd = {'ColorizerAttachToBuffer', 'ColorizerDetachFromBuffer', 'ColorizerReloadAllBuffers', 'ColorizerToggle'} }
	use { 'rktjmp/lush.nvim' }
	use { 'yashguptaz/calvera-dark.nvim' }
	use { 'catppuccin/nvim', as = 'catppuccin' }
	use { 'rose-pine/neovim', as = 'rose-pine', tag = '0.5.0' }
	use { 'sainnhe/edge' }
	use { 'sainnhe/everforest' }
	use { 'sainnhe/sonokai' }
	use { 'fenetikm/falcon' }
	use { 'RRethy/nvim-base16' }

	-- Utils
	use { 'nvim-lua/popup.nvim' }
	use { 'nvim-lua/plenary.nvim' }
	use { 'mfussenegger/nvim-dap' }
	use {'nvim-telescope/telescope-ui-select.nvim' }
	use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate', config = [[require('_treesitter')]] }
	use { 'nvim-telescope/telescope.nvim', requires = { "popup.nvim", "plenary.nvim", }, config = [[require('_telescope')]] }

	-- Pretty
	use { 'glepnir/dashboard-nvim', config = [[require('_dashboard')]] }
	use { 'simrat39/symbols-outline.nvim', config = [[require('symbol_tree')]] }
	use { 'akinsho/toggleterm.nvim', config = [[require('term')]], cmd = {'ToggleTerm', 'ToggleTermToggleAll', 'TermExec'} }
	use { 'hoob3rt/lualine.nvim', config = [[require('statusline')]] }
	use { 'ryanoasis/vim-devicons' }

	-- Keeping my sanity
	use { 'Raimondi/delimitMate' }
	use { 'tpope/vim-fugitive' }

	-- Cool Shit
	use { 'ThePrimeagen/harpoon', config = [[require('_harpoon')]] }

	-- Lisp
	use { 'vlime/vlime', rtp = 'vim/' }

	-- Lsp
	use {
		'VonHeikemen/lsp-zero.nvim',
		requires = {
			-- LSP Support
			{'neovim/nvim-lspconfig'},
			{'williamboman/mason.nvim'},
			{'williamboman/mason-lspconfig.nvim'},

			-- Autocompletion
			{'hrsh7th/nvim-cmp'},
			{'hrsh7th/cmp-buffer'},
			{'hrsh7th/cmp-path'},
			{'saadparwaiz1/cmp_luasnip'},
			{'hrsh7th/cmp-nvim-lsp'},
			{'hrsh7th/cmp-nvim-lua'},

			-- Snippets
			{'L3MON4D3/LuaSnip'},
			{'rafamadriz/friendly-snippets'},
		},
		config = [[require('_lsp')]]
	}

end)
