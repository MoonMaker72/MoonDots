require'nvim-treesitter.configs'.setup {
  ensure_installed = { "rust", "python", "css", "html", "javascript", "json", "lua", "toml", "yaml", "cpp", "c", "haskell", "commonlisp" },
  sync_install = false,
  highlight = {
    enable = true
  },
}

vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "nvim_treesitter#foldexpr()"
