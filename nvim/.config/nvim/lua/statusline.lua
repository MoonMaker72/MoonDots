local lualine = require 'lualine'

local colors = {
  bg       = '#232136',
  fg       = '#e0def4',
  red      = '#eb6f92',
  green    = '#9ccfd8',
  yellow   = '#f6c177',
  blue     = '#3e8fb0',
  magenta  = '#c4a7e7',
  cyan     = '#ea9a97',
  darkblue = '#31748f',
  orange   = '#ebbcba',
  violet   = '#c4a7e7',
}
local mode_color = {
  n = colors.red,
  i = colors.green,
  v = colors.blue,
  [''] = colors.blue,
  V = colors.blue,
  c = colors.magenta,
  no = colors.red,
  s = colors.orange,
  S = colors.orange,
  [''] = colors.orange,
  ic = colors.yellow,
  R = colors.violet,
  Rv = colors.violet,
  cv = colors.red,
  ce = colors.red,
  r = colors.cyan,
  rm = colors.cyan,
  ['r?'] = colors.cyan,
  ['!'] = colors.red,
  t = colors.red
}

local conditions = {
  buffer_not_empty = function() return vim.fn.empty(vim.fn.expand('%:t')) ~= 1 end,
  hide_in_width = function() return vim.fn.winwidth(0) > 80 end,
  check_git_workspace = function()
    local filepath = vim.fn.expand('%:p:h')
    local gitdir = vim.fn.finddir('.git', filepath .. ';')
    return gitdir and #gitdir > 0 and #gitdir < #filepath
  end
}

local config = {
  options = {
    component_separators = "",
    section_separators = "",
    theme =  {
		normal = {c = {fg = colors.fg}},
    	inactive = {a = {fg = colors.violet}, x = {fg = colors.violet}}
	}
  },
  sections = {
    lualine_a = {},
    lualine_b = {},
    lualine_y = {},
    lualine_z = {},
    lualine_c = {},
    lualine_x = {}
  },
  inactive_sections = {
    lualine_a = { 'filename' },
    lualine_v = {},
    lualine_y = {},
    lualine_z = {},
    lualine_c = {},
    lualine_x = { 'filetype' }
  }
}

local function ins_left(component)
  table.insert(config.sections.lualine_c, component)
end

local function ins_right(component)
  table.insert(config.sections.lualine_x, component)
end

ins_left {
	function()
		return ''
	end,
	color = {fg = colors.bg},
	padding = 0,
}

ins_left {
  function()
    vim.api.nvim_command('hi! LualineMode gui=italic guifg=' .. mode_color[vim.fn.mode()] .. " guibg=" .. colors.bg)
    return 'ﯶ'
  end,
  color = "LualineMode",
}

ins_left {
  'filename',
  condition = conditions.buffer_not_empty,
  color = {bg = colors.bg, gui = 'bold'},
}

ins_left {
  'branch',
  icon = '',
  condition = conditions.check_git_workspace,
  color = {bg = colors.bg},
}

ins_left {
	function()
		return ''
	end,
	color = {fg = colors.bg},
	padding = 0,
}

ins_right {
	function()
		return ''
	end,
	color = {fg = colors.bg},
	padding = 0,
}

ins_right {
  'diagnostics',
  sources = {'nvim_diagnostic'},
  symbols = {error = ' ', warn = ' ', info = ' '},
  color = {bg = colors.bg},
  color_error = colors.red,
  color_warn = colors.yellow,
  color_info = colors.cyan
}

ins_right {
  function()
    local msg = 'No Active Lsp'
    local buf_ft = vim.api.nvim_buf_get_option(0, 'filetype')
    local clients = vim.lsp.get_active_clients()
    if next(clients) == nil then return msg end
    for _, client in ipairs(clients) do
      local filetypes = client.config.filetypes
      if filetypes and vim.fn.index(filetypes, buf_ft) ~= -1 then
        return client.name
      end
    end
    return msg
  end,
  icon = ' LSP:',
  color = {bg = colors.bg},
}

ins_right {
  'filetype',
  upper = true,
  icons_enabled = true,
  left_padding = 1,
  right_padding = 0,
  color = {bg = colors.bg},
}

ins_right {
	'location',
	color = "LualineMode",
	padding = 2,
}

ins_right {
	function()
		return ''
	end,
	color = {fg = colors.bg},
	padding = 0,
}

lualine.setup(config)
