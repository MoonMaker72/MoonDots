-- * * *
-- Cmp Setup
-- * * *
local has_words_before = function()
  local line, col = unpack(vim.api.nvim_win_get_cursor(0))
  return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

local luasnip = require("luasnip")
local cmp = require("cmp")

cmp.setup({

	snippet = {
		expand = function(args)
			luasnip.lsp_expand(args.body)
		end,
	},

	window = {
		completion = cmp.config.window.bordered(),
		documentation = cmp.config.window.bordered(),
	},

	sources = cmp.config.sources({
		{ name = 'nvim_lsp' },
		{ name = 'luasnip' },
	}, {
		{ name = 'buffer' },
	}),

	mapping = {
	['<C-b>'] = cmp.mapping.scroll_docs(-4),
	['<C-f>'] = cmp.mapping.scroll_docs(4),
	['<C-Space>'] = cmp.mapping.complete(),
	['<C-e>'] = cmp.mapping.abort(),
	['<Enter>'] = cmp.mapping.confirm({ select = true }),
	["<C-n>"] = cmp.mapping(function(fallback)
		if cmp.visible() then
			cmp.select_next_item()
		elseif luasnip.expand_or_jumpable() then
			luasnip.expand_or_jump()
		elseif has_words_before() then
			cmp.complete()
		else
			fallback()
		end
	end, { "i", "s" }),
	["<C-p>"] = cmp.mapping(function(fallback)
		if cmp.visible() then
			cmp.select_prev_item()
		elseif luasnip.jumpable(-1) then
			luasnip.jump(-1)
		else
			fallback()
		end
	end, { "i", "s" }),

  },
})

local capabilities = require('cmp_nvim_lsp').default_capabilities(vim.lsp.protocol.make_client_capabilities())

-- * * *
-- Lsp Setup
-- * * *
local nvim_lsp = require('lspconfig')


require'lspconfig'.pyright.setup{ on_attach = custom_lsp_attach, capabilities = capabilities }
require'lspconfig'.clangd.setup{ on_attach = custom_lsp_attach, capabilities = capabilities }
require'lspconfig'.hls.setup{ on_attach = custom_lsp_attach, capabilities = capabilities }
require'lspconfig'.sumneko_lua.setup{ on_attach = custom_lsp_attach, capabilities = capabilities }

-- * * *
-- Rust Tools
-- * * *
local opts = {
	server = {
		on_attach = custom_lsp_attach,
		standalone = true,
	},
}

require('rust-tools').setup(opts)
