-- Edge Conifg
vim.g.edge_better_performance = 1
vim.g.edge_style = 'aura'
vim.g.edge_enable_italic = 1
vim.g.edge_transparent_background = 1
vim.g.edge_menu_selection_background = 'purple'
vim.g.edge_show_eob = 0

-- Everforest Config
vim.g.everforest_enable_italic = 1
vim.g.everforest_background = 'hard'
vim.g.everforest_transparent_background = 1
vim.g.everforest_cursor = 'red'
vim.g.everforest_better_performance = 1

-- Sonokai Config
vim.g.sonokai_style = 'andromeda'
vim.g.sonokai_enable_italic = 1
vim.g.sonokai_transparent_background = 1
vim.g.sonokai_better_performance = 1

-- Calvera Config
vim.g.calvera_style = 'shusia'
vim.g.calvera_enable_italic = true
vim.g.calvera_contrast = true
vim.g.calvera_transparent_bg = true
vim.g.calvera_border = true
vim.g.italic_comments = true

-- Falcon Config
vim.g.falcon_background = false

-- Rose-pine Config
vim.g.rose_pine_variant = 'moon'
vim.g.rose_pine_bold_vertical_split_line = true
vim.g.rose_pine_disable_background = true

-- Load the colorscheme
vim.cmd[[set background=dark]]
vim.cmd[[set termguicolors]]
vim.cmd[[colorscheme sonokai]]
