local map = vim.api.nvim_set_keymap
options = { noremap = true }

-- My Maps
map('n', 'S', ':nohls<CR>', options)
map('n', 'gn',    ':bn<CR>',    options)
map('n', 'gp',    ':bp<CR>',    options)
map('n', '<leader>fc', '<cmd>e $MYVIMRC<CR>', options)
map('n', '<leader>fn', '<cmd>enew<CR>', options)
map('n', '<leader>so', '<cmd>so %<CR>', options)
map('n', '<leader>nn', '<cmd>set nu!<CR>', options)
map('i', '.', '.<C-g>u', options)
map('i', '!', '!<C-g>u', options)
map('i', '?', '?<C-g>u', options)
map('i', ':', ':<C-g>u', options)
-- Telescope Maps
map('n', '<leader>ff', '<cmd>Telescope find_files<CR>', options)
map('n', '<leader>fg', '<cmd>Telescope live_grep<CR>', options)
map('n', '<leader>fb', '<cmd>Telescope buffers<CR>', options)
map('n', '<leader>fh', '<cmd>Telescope help_tags<CR>', options)
map('n', '<leader>fo', '<cmd>Telescope oldfiles<CR>', options)
-- Term Maps
map('n', '<leader>tt', '<cmd>ToggleTerm<CR>', options)
map('t', '<esc>', '<C-\\><C-n>', options)
-- SymbolTree Map
map('n', '<leader>ss', '<cmd>SymbolsOutline<CR>', options)
-- Limelight Map
map('n', '<leader>ll', '<cmd>Limelight<CR>', options)
-- Goyo Map
map('n', '<leader>gg', '<cmd>Goyo<CR>', options)
