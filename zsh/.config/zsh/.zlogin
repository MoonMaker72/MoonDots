if [ "$TERM" = "linux" ]; then
	if [[ "$(tty)" == "/dev/tty1" ]]; then
		startx
	else
		choices="Bspwm\nriver\nTTy"
		choice=$(echo $choices | sk --color=16,fg:2,current:6,pointer:9 --layout=reverse-list --margin 10% --header="Start Session")

		case $choice in
			TTy)
				echo "TTy"
				;;
			Bspwm)
				startx
				;;
			River)
				river
				;;
			*)
				echo "What?"
				;;
		esac
	fi
fi
