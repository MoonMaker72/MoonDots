#   __________        .__
#   \____    /  ______|  |__
#     /     /  /  ___/|  |  \
#    /     /_  \___ \ |   Y  \
#   /_______ \/____  >|___|  /
#           \/     \/      \/
#
#  Author: MoonMaker72
#  Repo: https://codeberg.org/MoonMaker72/MoonDots

# Prompt Customization
function git_check() {
	if git rev-parse --abbrev-ref 2> /dev/null ; then
		echo "%F{magenta}git::(%f%F{red}%B$(git branch --show-current)%b%f%F{magenta})%f"
	fi
}
function zle-line-init zle-keymap-select {
	ARW="${${KEYMAP/vicmd/ᛝ}/(main|viins)/ᛟ}"
	GIT_BR=$(git_check)
	RPROMPT="${GIT_BR}"
	PROMPT="%F{red}%B%~%f%b %F{%(0?.magenta.yellow)}$ARW%f "
	zle reset-prompt
}
zle -N zle-line-init
zle -N zle-keymap-select

# Enable colors
autoload -U colors && colors

# Other Completions
fpath=($XDG_DATA_HOME/zsh-completions $fpath)

# Autocomplete
autoload -Uz compinit
compinit
zstyle ':completion:*' menu select
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
zmodload zsh/complist
_comp_options+=(globdots)
setopt COMPLETE_ALIASES

# Vi mode
bindkey -v
export  KEYTIMEOUT=1

# Vim keys for autocomplete menu
bindkey -M menuselect 'h' 		vi-backward-char
bindkey -M menuselect 'j' 		vi-down-line-or-history
bindkey -M menuselect 'k' 		vi-up-line-or-history
bindkey -M menuselect 'l' 		vi-forward-char
bindkey -M menuselect 'left' 	vi-backward-char
bindkey -M menuselect 'down' 	vi-down-line-or-history
bindkey -M menuselect 'up' 		vi-up-line-or-history
bindkey -M menuselect 'right' 	vi-forward-char

# Backspace in insert after switching modes
bindkey "^?" backward-delete-char

# Edit line in nvim at <C-v>
autoload -U  edit-command-line
zle      -N  edit-command-line
bindkey '^v' edit-command-line

# Start zellij control
bindkey -s '^Z' '~/scripts/zelic.sh\n'

# Open xbps-menu
bindkey -s '^X' 'xbps-menu.sh\n'

# History
HISTFILE=~/.config/zsh/histfile
HISTSIZE=10000
SAVEHIST=10000

# Aliases
alias l=lsd
alias la="lsd -a"
alias wget="wget --hsts-file=$HOME/.cache/wget-hsts"
alias vim=nvim
alias v=nvim
alias cls="clear"
alias YTplaylist="youtube-dl -f \"bestaudio\" --continue --no-overwrites --ignore-errors --extract-audio --audio-format mp3 -o \"%(title)s.%(ext)s\""
alias Crun="cargo run"
alias Crunrel="cargo run --release"
alias PCrunrel="prime-run cargo run --release"
alias Cbuild="cargo build"
alias Cbuildrel="cargo build --release"
alias info="info --vi-keys"
alias socon="source $HOME/.config/zsh/.zshrc"
alias pmpv="prime-run mpv"
alias nmpv="prime-run mpv --profile=hiq"

# Zoxide init
eval "$(zoxide init zsh)"

# Syntax Highlighting & Autosuggestion & Repo Check
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh 2>/dev/null
