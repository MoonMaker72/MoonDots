#!/bin/sh

if [ ! $(pgrep -xfc "polybar top") -gt 0 ]; then
	polybar top &
fi
if [ ! $(pgrep -xfc "polybar bottom") -gt 0 ]; then
	polybar bottom &
fi
