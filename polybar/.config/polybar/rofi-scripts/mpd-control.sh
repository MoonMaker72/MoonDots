#!/bin/sh

opts="Play/Pause\nNext\nPrev\nStop"

choice=$(echo "$opts" | rofi -dmenu -p "Mpd Control:" -location 7 -xoffset 3 -yoffset -19 -theme-str '#window { width: 320; } #listview { lines: 7; }')

case $choice in
	"Play/Pause")
		mpc toggle
		;;
	"Next")
		mpc next
		;;
	"Prev")
		mpc prev
		;;
	"Stop")
		mpc stop
		;;
esac
