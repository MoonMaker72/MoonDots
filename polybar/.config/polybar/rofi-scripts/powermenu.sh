#!/bin/sh
menu="rofi -dmenu -p >> -location 3 -yoffset 19 -xoffset -3"
theme_str="#window { width: 320; } #listview { lines: 7; }"

opts="Quit\nLock\nShutdown\nReboot\nSleep"
choice=$(echo "$opts" | $menu -theme-str "$theme_str")

case $choice in
	Quit)
		bspc quit
		;;
	Lock)
		$HOME/scripts/xlocker.sh
		;;
	Shutdown)
		notify-send "Shutting Down" "System shutting down in 5 seconds" \
			&& sleep 5 \
			&& bspc quit \
			&& sleep 1 \
			&& sudo shutdown -h now
		;;
	Reboot)
		notify-send "Rebooting" "System Rebooting in 5 seconds" \
			&& sleep 5 \
			&& bspc quit \
			&& sleep 1 \
			&& sudo shutdown -r now
		;;
	Sleep)
		sudo zzz
		;;
esac
