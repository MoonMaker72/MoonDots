(
    max_notifications: 0,

    timeout: 9200,
    idle_threshold: 100,

    poll_interval: 16,

    history_length: 10,

    replacing_enabled: false,
    replacing_resets_timeout: false,

    debug: false,
    debug_color: Color(r: 0.0, g: 1.0, b: 0.0, a: 1.0),
    debug_color_alt: Color(r: 1.0, g: 0.0, b: 0.0, a: 1.0),

    print_to_file: "/tmp/wired.log",

    min_window_width: 1,
    min_window_height: 1,

    layout_blocks: [
        // -------- Root 1 ---------
        // Standard Text Notifications
        (
            name: "root",
            parent: "",
            hook: Hook(parent_anchor: TR, self_anchor: TR),
            offset: Vec2(x: -22.0, y: 20.0),
            render_anti_criteria: [Or([HintImage, Progress])],
            params: NotificationBlock((
                monitor: 0,
                border_width: 5.0,
                border_rounding: 6.0,
                background_color: Color(hex: "#1a181a"),
                border_color: Color(hex: "#ab9df2"),
                border_color_low: Color(hex: "#2d2a2e"),
                border_color_critical: Color(hex: "#eb6f92"),
                border_color_paused: Color(hex: "#e5c463"),

                gap: Vec2(x: 0.0, y: 16.0),
                notification_hook: Hook(parent_anchor: BL, self_anchor: TL),
            )),
        ),

        (
            name: "summary",
            parent: "root",
            hook: Hook(parent_anchor: TL, self_anchor: TL),
            offset: Vec2(x: 3.0, y: 0.0),
            params: TextBlock((
                text: "%s",
                font: "Metal Mania 32",
                ellipsize: Middle,
                color: Color(hex: "#ab9df2"),
                color_hovered: Color(hex: "#7accd7"),
                padding: Padding(left: 20.0, right: 20.0, top: 14.0, bottom: 2.0),
                dimensions: (width: (min: 120, max: 500), height: (min: 0, max: 0)),
            )),
        ),

        (
            name: "body",
            parent: "summary",
            hook: Hook(parent_anchor: BL, self_anchor: TL),
            offset: Vec2(x: 0.0, y: 0.0),
            params: ScrollingTextBlock((
                text: "%b",
                font: "Roboto Italic 16",
                color: Color(hex: "#e3e1e4"),
                color_hovered: Color(hex: "#ab9df2"),
                padding: Padding(left: 20.0, right: 20.0, top: 2.0, bottom: 20.0),
                width: (min: 320, max: 500),
                scroll_speed: 0.1,
                lhs_dist: 35.0,
                rhs_dist: 35.0,
                scroll_t: 1.0,
            )),
        ),

        // -------- Root 2 ---------
        // Progressbar Notifications
        (
            name: "root_progress",
            parent: "",
            hook: Hook(parent_anchor: BR, self_anchor: BR),
            offset: Vec2(x: -22.0, y: -20.0),
            render_criteria: [Or([Progress])],
            params: NotificationBlock((
                monitor: 0,
                border_width: 5.0,
                border_rounding: 6.0,
                background_color: Color(hex: "#1a181a"),
                border_color: Color(hex: "#ab9df2"),
                border_color_low: Color(hex: "#2d2a2e"),
                border_color_critical: Color(hex: "#eb6f92"),
                border_color_paused: Color(hex: "#e5c463"),

                gap: Vec2(x: 0.0, y: 16.0),
                notification_hook: Hook(parent_anchor: BL, self_anchor: TL),
            )),
        ),
        (
            name: "root_image",
            parent: "root_progress",
            hook: Hook(parent_anchor: TL, self_anchor: TL),
            offset: Vec2(x: 0.0, y: 0.0),
            params: ImageBlock((
                image_type: Hint,
                padding: Padding(left: 24.0, right: 6.0, top: 12.0, bottom: 2.0),
                rounding: 3.0,
                scale_width: 64,
                scale_height: 64,
                filter_mode: Triangle,
            )),
        ),
        (
            name: "progress_summary",
            parent: "root_image",
            hook: Hook(parent_anchor: MR, self_anchor: ML),
            offset: Vec2(x: 0.0, y: 0.0),
            params: TextBlock((
                text: "%s",
                font: "Quantico Bold Italic 35",
                ellipsize: Middle,
                color: Color(hex: "#ab9df2"),
                color_hovered: Color(hex: "#7accd7"),
                padding: Padding(left: 14.0, right: 14.0, top: 2.0, bottom: 0.0),
                dimensions: (width: (min: 120, max: 300), height: (min: 0, max: 0)),
            )),
        ),
        (
            name: "progress",
            parent: "root_image",
            hook: Hook(parent_anchor: BL, self_anchor: TL),
            offset: Vec2(x: 0.0, y: 0.0),
            params: ProgressBlock((
                padding: Padding(left: 12.0, right: 12.0, top: 6.0, bottom: 12.0),
                border_width: 5.0,
                width: 250,
                height: 24,
                border_rounding: 3.0,
                fill_rounding: 1.0,
                border_color: Color(hex: "#ab9df2"),
                background_color: Color(hex: "#e5c463"),
                fill_color: Color(hex: "#f85e84"),
                size: Vec2(x: 100.0, y: 30.0),
                border_color_hovered: Color(hex: "#ab9df2"),
                background_color_hovered: Color(hex: "#e5c463"),
                fill_color_hovered: Color(hex: "#f85e84"),
            )),
        ),

        // -------- Root 3 ---------
        // Image Notifications
    ],

    shortcuts: ShortcutsConfig (
        notification_interact: 1,
        notification_close: 2,
        notification_action1: 3,
    ),
)
